# **Kaggle - Salt Segmentation**

Project created for the Kaggle competition.  
Link to the competition: https://www.kaggle.com/c/tgs-salt-identification-challenge

## **Getting Started**

### **Prerequisites**

Neededed dependencies:
*  python3.6
*  pickle
*  pandas
*  numpy
*  matplotlib
*  seaborn
*  sklearn
*  tqdm
*  skimage
*  tensorflow

### **Running**  
>>>
```sh
python Salt_Detection.py 
python crf_correction_sub.py
```
>>>